import { defineConfig } from 'vite'
import vue from '@vitejs/plugin-vue'
import { fileURLToPath, URL } from 'node:url'

export default defineConfig({
  plugins: [vue()],
  resolve: {
    alias: {
      '@': fileURLToPath(new URL('./src', import.meta.url))
    }
  },
  server: {
    fs: {
      allow: [
        './', // allow serving all files in the current directory
        '../node_modules/bootstrap-icons/font/fonts/' // allow serving the bootstrap-icons fonts
      ]
    }
  }
  
  
});
